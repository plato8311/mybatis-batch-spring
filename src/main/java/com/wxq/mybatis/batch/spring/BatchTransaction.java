package com.wxq.mybatis.batch.spring;

import org.apache.ibatis.transaction.Transaction;
import org.mybatis.logging.Logger;
import org.mybatis.logging.LoggerFactory;
import org.springframework.jdbc.datasource.ConnectionHolder;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.util.Assert;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author weixiaoqiang
 * @date 2024/5/22
 **/
public class BatchTransaction implements Transaction {

    private static final Logger logger = LoggerFactory.getLogger(BatchTransaction.class);

    private DataSource dataSource;

    private Connection connection;

    private boolean isConnectionTransactional;

    public BatchTransaction(DataSource dataSource) {
        Assert.notNull(dataSource, "No dataSource specified");
        this.dataSource = dataSource;
    }

    @Override
    public Connection getConnection() throws SQLException {
        if(this.connection == null) {
            this.openConnection();
        }
        return connection;
    }

    @Override
    public void commit() throws SQLException {
        if(isConnectionTransactional) {
            throw new SQLException("The current transaction has been managed by spring");
        }
        if(this.connection != null) {
            logger.debug(() -> "Committing JDBC Connection [" + this.connection + "]");
            this.connection.commit();
        }
    }

    @Override
    public void rollback() throws SQLException {
        if(isConnectionTransactional) {
            throw new SQLException("The current transaction has been managed by spring");
        }
        if(this.connection != null) {
            logger.debug(() -> "Rolling back JDBC Connection [" + this.connection + "]");
            this.connection.rollback();
        }
    }

    @Override
    public void close() throws SQLException {
        DataSourceUtils.releaseConnection(this.connection, dataSource);
    }

    @Override
    public Integer getTimeout() throws SQLException {
        ConnectionHolder holder = (ConnectionHolder) TransactionSynchronizationManager.getResource(this.dataSource);
        return holder != null && holder.hasTimeout() ? holder.getTimeToLiveInSeconds() : null;
    }

    private void openConnection() throws SQLException {
        this.connection = DataSourceUtils.getConnection(this.dataSource);
        this.connection.setAutoCommit(false);
        this.isConnectionTransactional = DataSourceUtils.isConnectionTransactional(this.connection, this.dataSource);
        logger.debug(() -> "JDBC Connection [" + this.connection + "] will"
                + (this.isConnectionTransactional ? " " : " not ") + "be managed by Spring");
    }
}
